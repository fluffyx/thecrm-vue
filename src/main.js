// Import Vue
import Vue from 'vue';

// Import F7
import Framework7 from 'framework7/dist/js/framework7.js';

// Import F7 Vue Plugin
import Framework7Vue from 'framework7-vue/dist/framework7-vue.js';

import VueLocalStorage from 'vue-localstorage';

import InfiniteScroll from 'v-infinite-scroll';

// You need a specific loader for CSS files like https://github.com/webpack/css-loader
import 'v-infinite-scroll/dist/v-infinite-scroll.css';
 

// Import Routes
import Routes from './routes.js';

// Import App Component
import App from './app.vue';

// Init F7 Vue Plugin
Vue.use(Framework7Vue, Framework7);
Vue.use(VueLocalStorage);
Vue.use(InfiniteScroll);

// Init App
export default new Vue({
  el: '#app',
  template: '<app/>',
  // Init Framework7 by passing parameters here
  framework7: {
    id: 'io.framework7.testapp', // App bundle ID
    name: 'Framework7', // App name
    theme: 'ios', // Automatic theme detection
    // App routes
    routes: Routes,
  },
  panel: {
    leftBreakpoint: 960
  },
  // Register App Component
  components: {
    app: App
  },
  methods: {
   
  }
});


// var replyrequest;

// $(document).on('click', '.contact-link', function(){
//   $('.contact-link').parent().removeClass('selected');
//   $(this).parent().addClass('selected');
//   clearInterval(replyrequest);
// });





