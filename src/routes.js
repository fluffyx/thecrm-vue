import ConversationPage from './pages/conversation.vue';
import HomePage from './pages/home.vue';
import LoginPage from './pages/login.vue';
import ConversationinfoPage from './pages/conversationinfo.vue';
import Conversationlist from './pages/conversationlist.vue';
import ContactsPage from './pages/contacts.vue';
import ContactinfoPage from './pages/contactinfo.vue';
import ContacteditPage from './pages/contactedit.vue';
import NotFoundPage from './pages/not-found.vue';
import TempPage from './pages/temp.vue';

export default [
  {
    path: '/',
    component: HomePage,
  },
  {
    path: '/temp',
    component: TempPage,
  },
  {
    path: '/login',
    component: LoginPage,
  },
  {
    path: '/conversation/:contactid',
    component: ConversationPage,
  },  
  {
    path: '/conversationinfo/:contactid',
    component: ConversationinfoPage,
  },
  {
    path: '/conversationlist/',
    component: Conversationlist,
  },
  {
    path: '/contacts/',
    component: ContactsPage,
  },
  {
    path: '/contactinfo/:contactid',
    component: ContactinfoPage,
  },
  {
    path: '/contactedit/:contactid',
    component: ContacteditPage,
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];
